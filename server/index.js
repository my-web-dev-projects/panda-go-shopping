const dotenv = require("dotenv")
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()

dotenv.config({path:"./server/config.env"})

require("./db/conn");
// const User = require("./model/userSchema");

// Convert json data into object
app.use(express.json());

const apiPort = process.env.PORT;

// Middleware
const middleware = (req, res, next) => {
    console.log("Middleware has been initialized");
    next();
}

app.use(require("./router/auth"));
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
app.use(bodyParser.json())

app.listen(apiPort, () => console.log(`Server running on port localhost:${apiPort}`))