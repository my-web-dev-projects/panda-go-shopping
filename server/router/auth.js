const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const router = express.Router();

require("../db/conn");
const User = require("../model/userSchema");

router.get("/", (req, res) => {
    res.send("Hello World!")
})

// Registration Route by using promises
router.post("/register", (req, res) => {
    const { name, email, phone, password, cpassword } = req.body;

    if (!name || !email || !phone || !password || !cpassword) {
        return res.status(422).json({ error: "Please check if all the inputs are filled." })
    }

    User.findOne({ email: email }).then((userExist) => {
        if (userExist) {
            return res.status(422).json({ error: "User already exists!" })
        }
        else if (password != cpassword) {
            return res.status(422).json({ error: "Passwords are not matching!" })
        }
        else {
            const user = new User({
                name,
                email,
                phone,
                password,
                cpassword
            })
            user.save().then(() => {
                res.status(201).json({ message: "New User Added Successfully!" })
            }).catch((err) => res.status(500).json({ error: `${err}` }));
        }
    }).catch(err => console.log(err));
})

// Login Route by using async-await
router.post("/signin", async (req, res) => {
    try {
        const { email, password } = req.body;

        if (!email || !password) {
            return res.status(400).json({ error: "Please fill the required fields" });
        }

        const userLogin = await User.findOne({ email: email });

        if (userLogin) {

            // Verify hashed password at the time of the login 
            const isMatch = await bcrypt.compare(password, userLogin.password)

            const token = await userLogin.generateAuthToken();
            // storing token in cookie
            res.cookie("jwtoken",token,{
                // Expire user token in 30 days
                expires:new Date(Date.now() + 25892000000),
                httpOnly:true
            });

            if (!isMatch) {
                res.status(400).json({ error: "Invalid Credientials" })
            } else {
                res.json({ message: `Hello ${userLogin.name}!` })
            }
        } else {
            res.status(400).json({ error: "Invalid Credientials" })
        }

    } catch (err) {
        console.log(err)
    }
})

module.exports = router;