import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import { PandaCEA } from '../panda-close-eyes-animation/pandaCEA'
import Logo from "../../assets/logo.svg"
import "./styleLogin.css";

export const Register = () => {
    const [isEyesClose, setIsEyesClose] = useState(false);

    const handlePasswordFocus = () => {
        setIsEyesClose(true)
    }

    const handlePasswordBlur = () => {
        setIsEyesClose(false);
    };

    return (
        <div className="reg-body">
            <img src={Logo} alt="" />
            <div className="reg-container">
                <div>
                    <PandaCEA isEyesClose={isEyesClose} svgWidth={200} svgHeight={150} />
                </div>
                <div className="form">
                <div className="inputBox">
                        <input type="text" required />
                        <span>Name</span>
                    </div>
                    <div className="inputBox">
                        <input type="text" required />
                        <span>E - mail</span>
                    </div>
                    <div className="inputBox">
                        <input type="tel" required />
                        <span>Phone Number</span>
                    </div>
                    <div class="inputBox">
                        <input type="password" required onFocus={handlePasswordFocus} onBlur={handlePasswordBlur} />
                        <span>Password</span>
                    </div>
                    <div class="inputBox">
                        <input type="password" required onFocus={handlePasswordFocus} onBlur={handlePasswordBlur} />
                        <span>Confirm Password</span>
                    </div>
                    <button className="submit-btn">Register</button>
                </div>
                <span>Already have an account? <Link className="style-links" to = "/login"  >Login Here Buddy!</Link></span>
            </div>
        </div>
  )
}
