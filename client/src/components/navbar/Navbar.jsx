import React from 'react'
import { Link } from "react-router-dom";
import Logo from "../../assets/Face-happy(logo).svg"
import Name from "../../assets/ShopPanda.svg"
import SearchSVG from "../../assets/search.svg"
import UserMale from "../../assets/user-male.svg"
import Cart from "../../assets/cart.svg"
import "./styleNav.css"

export const Navbar = () => {
  return (
    <>
    <div className="header">
      <nav className='primaryNav'>
        <div className="navLogo">
          <img className='Logo' src={Logo} alt="" />
          <img className='Name mobile-hidden' src={Name} alt="" />
        </div>
        <div className="navSearch">
          <input type="text" name="search" id="search" />
          <button>{<img src={SearchSVG} alt="" />}</button>
        </div>
        <div className="navUser">
        <img className='mobile-hidden' src={UserMale} alt="" />
        <img className="mobile-hidden" src={Cart} alt="" />
        </div>
      </nav>
      <nav className="secondaryNav">
        <ul>
          <li>
            <a href="#">test1</a>
          </li>
          <li>
          <a href="#">test2</a>
          </li>
          <li>
          <a href="#">test3</a>
          </li>
        </ul>
      </nav>
    </div>
    </>
  )
}
